import money, json, websocket, time, hmac, hashlib, requests
from decimal import Decimal
from PyQt4 import Qt

name = 'HitBTC'

from money import BTC, ETH, EUR, GBP, LTC, NXT, USD
#BTC = money.ILCCCurrency(money.BTC, 9)
#ETH = money.ILCCCurrency(money.ETH, 9)
#EUR = money.ILCCCurrency(money.EUR, 7)
#GBP = money.ILCCCurrency(money.GBP, 7)
#LTC = money.ILCCCurrency(money.LTC, 9)
#NXT = money.ILCCCurrency(money.NXT, 9)
#USD = money.ILCCCurrency(money.USD, 7)
currencies = [BTC, ETH, EUR, GBP, LTC, NXT, USD]
#TODO: DOGE, XMR, BCN, XDN, FCN, DSH, NBT
currencydict = dict([(currency.code, currency) for currency in currencies])

BTCEUR = money.Pair(BTC, EUR)
BTCUSD = money.Pair(BTC, USD)
ETHBTC = money.Pair(ETH, BTC)
EURGBP = money.Pair(EUR, GBP)
EURUSD = money.Pair(EUR, USD)
GBPUSD = money.Pair(GBP, USD)
LTCBTC = money.Pair(LTC, BTC)
LTCEUR = money.Pair(LTC, EUR)
LTCUSD = money.Pair(LTC, USD)
NXTBTC = money.Pair(NXT, BTC)
pairs = [BTCEUR, BTCUSD, ETHBTC, EURGBP, EURUSD, GBPUSD, LTCBTC, LTCEUR, LTCUSD, NXTBTC]
#TODO: BCNBTC, DOGEBTC, DSHBTC, FCNBTC, NBTBTC, QCNBTC, XDNBTC, XMRBTC
pairdict = dict([(pair.code, pair) for pair in pairs])

class Order(money.Order):
    def __init__(self, order):
        pair = pairdict[order['symbol']]
        # TODO is order always 0.01 ?
        left = money.Money(Decimal(order['orderQuantity'])*Decimal('0.01'), pair.left)
        price = money.Money(Decimal(order['orderPrice']), pair.right)
        right = money.Money(left.amount*price.amount, pair.right)
        if order['side'] == 'sell':
            left = -left
        else:
            right = -right
        money.Order.__init__(self, pair, left, price, right)


class Trade:
    def __init__(self, keyfile='hitkey'):
        f = open(keyfile, 'r')
        self.key = f.readline().strip()
        self.secret = f.readline().strip()
        f.close()

    def api_trading(self, command):
        url = '/api/1/trading/{}?apikey={}&nonce={}'.format(
            command, self.key, int(time.time()*1000)
        )
        h = hmac.new(self.secret, url, hashlib.sha512)
        head = {'X-Signature': h.hexdigest()}
        return requests.get('https://api.hitbtc.com'+url, headers=head).json()

    def balance(self):
        return self.api_trading('balance')['balance']

    def balances(self):
        bs = self.api_trading('balance')['balance']
        wallets = []
        for b in bs:
            try:
                currency = currencydict[b['currency_code']]
            except:
                continue

            wallets += [
                money.Wallet('available', money.Money(Decimal(b['cash']), currency)),
                money.Wallet('locked', money.Money(Decimal(b['reserved']), currency))
            ]
        return wallets

    def openorders(self):
        orders = self.api_trading('orders/active')['orders']
        return [Order(order) for order in orders]

    def fees(self, pair):
        symboldata = requests.get('https://api.hitbtc.com/api/1/public/symbols').json(parse_float=Decimal)['symbols']
        for symbol in symboldata:
            if symbol['symbol'] == pair.code:
                return {'taker': symbol['takeLiquidityRate'], 'maker': symbol['provideLiquidityRate']}
        raise(False)

class Thread(Qt.QThread):            
    updated = Qt.pyqtSignal()

    def __init__(self, pair, parent=None):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.pairname = str(pair)
        self.asks = {}
        self.bids = {}
        self.ask = None
        self.bid = None
        self.ws = websocket.create_connection("ws://api.hitbtc.com/")
        self.synced = False
        self.exiting = False
        
    def run(self):
        while not self.exiting:
            s = self.ws.recv()
            data = json.loads(s, parse_float=Decimal)
            key = data.keys()[0]
            data = data[key]
            if data['symbol'] == self.pairname:
                if key == "MarketDataSnapshotFullRefresh":
                    self.asks.clear()
                    for w in data['ask']:
                        self.asks[w['price']] = w['size']

                    self.bids.clear()
                    for w in data['bid']:
                        self.bids[w['price']] = w['size']

                    self.synced = True
                    self.update()
                elif key == "MarketDataIncrementalRefresh":
                    if self.synced:
                        for w in data['ask']:
                            if w['size'] == 0:
                                del self.asks[w['price']]
                            else:
                                self.asks[w['price']] = w['size']
                        for w in data['bid']:
                            if w['size'] == 0:
                                del self.bids[w['price']]
                            else:
                                self.bids[w['price']] = w['size']
                        self.update()
                else:
                    print '[HitbtcThread] Unknown key', key
        self.ws.close()

    def update(self):
        self.ask = min([float(key) for key in self.asks.keys()])
        self.bid = max([float(key) for key in self.bids.keys()])
        self.updated.emit()
        
