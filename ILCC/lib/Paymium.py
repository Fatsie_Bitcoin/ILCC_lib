import requests, money
from sanction import Client
from decimal import Decimal
from PyQt4 import Qt

name = 'Paymium'

from money import BTC, EUR
#BTC = money.ILCCCurrency(money.BTC, 8)
#EUR = money.ILCCCurrency(money.EUR, 5)
currencies = [BTC, EUR]

BTCEUR = money.Pair(BTC, EUR)
pairs = [BTCEUR]

def ticker(pair=BTCEUR):
    assert(pair == BTCEUR)
    data = requests.get("https://paymium.com/api/v1/data/eur/ticker").json(parse_float=Decimal)
    return {'ask': data['ask'], 'bid': data['bid']}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()

    def __init__(self, pair=BTCEUR, parent=None):
        Qt.QThread.__init__(self, parent)
        self.ask = None
        self.bid = None
        self.exiting = False

    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker()
                ask = float(data["ask"])
                if ask != self.ask:
                    self.ask = ask
                    changed = True
                bid = float(data["bid"])
                if bid != self.bid:
                    self.bid = bid
                    changed = True
            except:
                self.bid = None
                self.ask = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(5)


class Order(money.Order):
    """
    Order class with Paymium specific extensions
    """

    def __init__(self, order):
        pair = BTCEUR
        left = money.Money(order['amount'], pair.left)
        price = money.Money(order['price'], pair.right)
        right = money.Money(left.amount*price.amount, pair.right)
        if order['direction'] == 'sell':
            left = -left
        else:
            right = -right
        money.Order.__init__(self, pair, left, price, right)


class Trade(Client):
    def __init__(self, keyfile = 'paymiumkey'):
        f = open(keyfile,'r')
        Client.__init__(
            self, token_endpoint="https://paymium.com/api/oauth/token",
            client_id = f.readline().strip(),
            client_secret = f.readline().strip()
        )
        self.refresh_token = f.readline().strip()
        f.close()       
        self.keyfile = keyfile

        self.refresh()
        
    def refresh(self):
        Client.refresh(self)
        f = open(self.keyfile,'w')
        f.write("{0}\n{1}\n{2}\n".format(self.client_id, self.client_secret, self.refresh_token))
        f.close()

    def openorders(self):
        orders = requests.get(
            "https://paymium.com/api/v1/user/orders?active=true",
            headers={"Authorization": "Bearer "+self.access_token}
        ).json()
        return [Order(order) for order in orders]

    def balances(self):
        b = requests.get(
            "https://paymium.com/api/v1/user",
            headers={"Authorization": "Bearer "+self.access_token}
        ).json(parse_float=Decimal)
        return [
            money.Wallet('available', money.Money(b['balance_btc'], BTC)),
            money.Wallet('locked', money.Money(b['locked_btc'], BTC)),
            money.Wallet('available', money.Money(b['balance_eur'], EUR)),
            money.Wallet('locked', money.Money(b['locked_eur'], EUR))
        ]

    def fees(self, pair):
        # pair ignored as it is general fee
        return {'taker': Decimal('0.0059'), 'maker': Decimal('0.0059')}
