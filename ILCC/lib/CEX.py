import cexapi, money
from decimal import Decimal
from PyQt4 import Qt

name = 'CEX'

api = cexapi.API('', '', '')

from money import BTC, ETH, EUR, LTC, NMC, RUB, USD
#BTC = money.ILCCCurrency(money.BTC, 8)
#ETH = money.ILCCCurrency(money.ETH, 8)
#EUR = money.ILCCCurrency(money.EUR, 2)
#LTC = money.ILCCCurrency(money.LTC, 8)
#NMC = money.ILCCCurrency(money.NMC, 8)
#RUB = money.ILCCCurrency(money.RUB, 2)
#USD = money.ILCCCurrency(money.USD, 2)
GHS = money.Currency('GHS', 'Nil', 'GH/s mining', [])
currencies = [BTC, ETH, EUR, GHS, LTC, NMC, RUB, USD]
# TODO: Other currencies
currencydict = dict([(currency.code, currency) for currency in currencies])

BTCUSD = money.Pair(BTC, USD, "BTC/USD")
BTCEUR = money.Pair(BTC, EUR, "BTC/EUR")
BTCRUB = money.Pair(BTC, RUB, "BTC/RUB")
ETHBTC = money.Pair(ETH, BTC, "ETH/BTC")
ETHUSD = money.Pair(ETH, USD, "ETH/USD")
GHSBTC = money.Pair(GHS, BTC, "GHS/BTC")
LTCUSD = money.Pair(LTC, USD, "LTC/USD")
LTCEUR = money.Pair(LTC, EUR, "LTC/EUR")
LTCBTC = money.Pair(LTC, BTC, "LTC/BTC")
pairs = [BTCUSD, BTCEUR, BTCRUB, ETHBTC, ETHUSD, GHSBTC, LTCUSD, LTCEUR, LTCBTC]
pairdict = dict([(pair.code, pair) for pair in pairs])

def ticker(pair=BTCEUR):
    data = api.ticker(str(pair)) 
    return {'ask': Decimal(data['ask']), 'bid': Decimal(data['bid'])}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()
    
    def __init__(self, pair, parent=None):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.ask = None
        self.bid = None
        self.exiting = False
        
    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker(self.pair)
                ask = float(data['ask'])
                if ask != self.ask:
                    self.ask = ask
                    changed = True
                bid = float(data['bid'])
                if bid != self.bid:
                    self.bid = bid
                    changed = True
            except:
                self.ask = None
                self.bid = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(3)


class Order(money.Order):
    def __init__(self, order):
        pair = pairdict[order['symbol1']+'/'+order['symbol2']]
        left = money.Money(Decimal(order['amount']), pair.left)
        price = money.Money(Decimal(order['price']), pair.right)
        right = money.Money(left.amount*price.amount, pair.right)
        if order['type'] == 'sell':
            left = -left
        else:
            right = -right
        money.Order.__init__(self, pair, left, price, right)


class Trade:
    def __init__(self, keyfile='cexkey'):
        f = open(keyfile)
        username = f.readline().strip()
        key = f.readline().strip()
        secret = f.readline().strip()
        self.api = cexapi.API(username, key, secret)

    def openorders(self):
        orders = self.api.current_orders('')
        return [Order(order) for order in orders]

    def balance(self):
        return self.api.balance()

    def balances(self):
        bs = self.api.balance()
        wallets = []
        for code, balances in bs.iteritems():
            try:
                currency = currencydict[code]
            except:
                continue
            
            for name, amount in balances.iteritems():
                wallets.append(money.Wallet(name, money.Money(Decimal(amount), currency)))
        
        return wallets

    def fees(self, pair):
        fee = self.api.api_call('get_myfee', private=1)['data']['{}:{}'.format(pair.left.code, pair.right.code)]
        assert(fee['buy'] == fee['sell'])
        f = Decimal(fee['buy'])/100
        return {'maker': f, 'taker': f}
        
