import requests, money
import vircurex
from decimal import Decimal
from PyQt4 import Qt

name = 'Vircurex'

from money import BTC, EUR, LTC, NMC, USD
#BTC = money.ILCCCurrency(money.BTC, 8)
#EUR = money.ILCCCurrency(money.EUR, 8)
#LTC = money.ILCCCurrency(money.LTC, 8)
#NMC = money.ILCCCurrency(money.NMC, 8)
#USD = money.ILCCCurrency(money.USD, 8)
currencies = [BTC, EUR, LTC, NMC, USD]
#TODO: PPC, FRC, DOGE, XPM, DGC, BC, TRC, ANC, DVC, FTC, QRK
currencydict = dict([(currency.code, currency) for currency in currencies])

BTCEUR = money.Pair(BTC, EUR, 'BTC_EUR')
BTCLTC = money.Pair(BTC, LTC, 'BTC_LTC')
BTCNMC = money.Pair(BTC, NMC, 'BTC_NMC')
BTCUSD = money.Pair(BTC, USD, 'BTC_USD')
EURBTC = money.Pair(EUR, BTC, 'EUR_BTC')
EURLTC = money.Pair(EUR, LTC, 'EUR_LTC')
EURNMC = money.Pair(EUR, NMC, 'EUR_NMC')
EURUSD = money.Pair(EUR, USD, 'EUR_USD')
LTCBTC = money.Pair(LTC, BTC, 'LTC_BTC')
LTCEUR = money.Pair(LTC, EUR, 'LTC_EUR')
LTCNMC = money.Pair(LTC, NMC, 'LTC_NMC')
LTCUSD = money.Pair(LTC, USD, 'LTC_USD')
NMCBTC = money.Pair(NMC, BTC, 'NMC_BTC')
NMCEUR = money.Pair(NMC, EUR, 'NMC_EUR')
NMCLTC = money.Pair(NMC, LTC, 'NMC_LTC')
NMCUSD = money.Pair(NMC, USD, 'NMC_USD')
USDBTC = money.Pair(USD, BTC, 'USD_BTC')
USDEUR = money.Pair(USD, EUR, 'USD_EUR')
USDLTC = money.Pair(USD, LTC, 'USD_LTC')
USDNMC = money.Pair(USD, NMC, 'USD_NMC')
pairs = [
    BTCEUR, BTCLTC, BTCNMC, BTCUSD,
    EURBTC, EURLTC, EURNMC, EURUSD,
    LTCBTC, LTCEUR, LTCNMC, LTCUSD,
    NMCBTC, NMCEUR, NMCLTC, NMCUSD,
    USDBTC, USDEUR, USDLTC, USDNMC
]
#TODO: All other; vircurex support all combinations
pairdict = dict([(pair.code, pair) for pair in pairs])

def ticker(pair):
    vp = vircurex.Pair(str(pair))
    #TODO: #digits
    return {'ask': Decimal(vp.lowest_ask), 'bid': Decimal(vp.highest_bid)}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()

    def __init__(self, pair, parent=None):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.ask = None
        self.bid = None
        self.exiting = False

    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker(self.pair)
                ask = float(data['ask'])
                if ask != self.ask:
                    self.ask = ask
                    changed = True
                bid = float(data['bid'])
                if bid != self.bid:
                    self.bid = bid
                    changed = True
            except:
                self.ask = None
                self.bid = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(5)

class Trade(vircurex.Account):
    def __init__(self, keyfile='virkey'):
        f = open(keyfile, 'r')
        self.account = f.readline().strip()
        self.secret = f.readline().strip()
        f.close
        vircurex.Account.__init__(self, self.account, self.secret)

    def balances(self):
        bs = vircurex.Account.balances(self)
        wallets = []
        for code, data in bs.iteritems():
            try:
                currency = currencydict[code]
            except:
                continue

            total = Decimal(data['balance'])
            available = Decimal(data['availablebalance'])
            locked = total - available

            wallets += [
                money.Wallet('available', money.Money(available, currency)),
                money.Wallet('locked', money.Money(locked, currency))
            ]
                
        return wallets

    def openorders(self):
        info = self.orders()
        orders = []
        for i in range(info['numberorders']):
            o = info.get('order-{}'.format(i+1))
            code = o['currency1']+'_'+o['currency2']
            pair = pairdict[code]
            left = money.Money(Decimal(o['quantity']), pair.left)
            price = money.Money(Decimal(o['unitprice']), pair.right)
            right = money.Money(left.amount*price.amount, pair.right)
            if o['ordertype'] == 'SELL':
                left = -left
            else:
                right = -right
            orders.append(money.Order(pair, left, price, right))
        return orders

    def fees(self, pair):
        # pair ignored, vircurex has one global fee
        fee = Decimal(requests.get("https://vircurex.com/api/get_trading_fee.json").json()['fee'])
        return {
            'taker': fee,
            'maker': fee
        }
