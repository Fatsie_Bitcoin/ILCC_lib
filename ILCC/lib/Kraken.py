import krakenex, money
from decimal import Decimal, ROUND_HALF_UP
from PyQt4 import Qt

name = 'Kraken'

from money import BTC, CAD, ETH, EUR, GBP, JPY, LTC, NMC, USD
FEE = money.Currency('FEE', 'Nil', 'Kraken fee', [])
currencies = [BTC, CAD, ETH, EUR, GBP, JPY, LTC, NMC, USD]
# TODO: KFEE, XXDG, XXLM, XXRP, XXVN, ZKRW
currencydict = {
    "XXBT": BTC,
    "XETH": ETH,
    "ZEUR": EUR,
    "KFEE": FEE,
    "XLTC": LTC
}

BTCCAD = money.Pair(BTC, CAD, 'XBTCAD')
BTCEUR = money.Pair(BTC, EUR, 'XBTEUR')
BTCGBP = money.Pair(BTC, GBP, 'XBTGBP')
BTCNMC = money.Pair(BTC, NMC, 'XBTNMC')
BTCJPY = money.Pair(BTC, JPY, 'XBTJPY')
BTCUSD = money.Pair(BTC, USD, 'XBTUSD')
BTCLTC = money.Pair(BTC, LTC, 'XBTLTC')
ETHBTC = money.Pair(ETH, BTC, 'ETHXBT')
ETHCAD = money.Pair(ETH, CAD)
ETHEUR = money.Pair(ETH, EUR)
ETHGBP = money.Pair(ETH, GBP)
ETHJPY = money.Pair(ETH, JPY)
ETHUSD = money.Pair(ETH, USD)
LTCCAD = money.Pair(LTC, CAD)
LTCEUR = money.Pair(LTC, EUR)
LTCUSD = money.Pair(LTC, USD)
pairs = [
    BTCCAD, BTCEUR, BTCGBP, BTCNMC, BTCJPY, BTCUSD, BTCLTC,
    ETHBTC, ETHCAD, ETHEUR, ETHGBP, ETHJPY, ETHUSD,
    LTCCAD, LTCEUR, LTCUSD
]
# TODO: *.d(?), XXBTXXLM, XXBTXXRP, XXBTXXDG
pairdict = dict([(pair.code, pair) for pair in pairs])

api = krakenex.API()

def ticker(pair):
    t = api.query_public('Ticker', req = {'pair': str(pair)})['result']
    assert(len(t) == 1)
    d = t.values()[0]
    return {'ask': Decimal(d['a'][0]), 'bid': Decimal(d['b'][0])}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()

    def __init__(self, pair, parent=None, invert=False):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.invert = invert
        self._ask = self.ask = None
        self._bid = self.bid = None
        self.exiting = False

    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker(self.pair)
                ask = float(data['ask'])
                if ask != self._ask:
                    self._ask = ask
                    if not self.invert:
                        self.ask = ask
                    else:
                        self.bid = 1/ask
                    changed = True
                bid = float(data['bid'])
                if bid != self._bid:
                    self._bid = bid
                    if not self.invert:
                        self.bid = bid
                    else:
                        self.ask = 1/bid
                    changed = True
            except:
                self.ask = None
                self.bid = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(5)

class Order(money.Order):
    """
    Order class with Kraken specific extensions
    """

    def __init__(self, txid, order):
        self.txid = txid
        desc = order['descr']
        pair = pairdict[desc['pair']]
        left = money.Money(order['vol'], pair.left)
        price = money.Money(desc['price'], pair.right)
        # TODO: properly round the right value
        right = money.Money(
            (left.amount*price.amount).quantize(Decimal('0.00000001'), ROUND_HALF_UP),
            pair.right
        )
        if desc['type'] == u'sell':
            left = -left
        else:
            right = -right
        money.Order.__init__(self, pair, left, price, right)

class Trade(krakenex.API):
    def __init__(self, keyfile='krkey'):
        krakenex.API.__init__(self)
        if keyfile:
            self.load_key(keyfile)

    def openorders(self):
        orders = self.query_private('OpenOrders')['result']['open']
        return [Order(id, order) for id, order in orders.iteritems()]

    def balances(self):
        """
        Gives the balances
        """
        bs = self.query_private('Balance')['result']
        return [
            money.Wallet('Total', money.Money(Decimal(amount), currencydict[code]))
            for code, amount in bs.iteritems()
        ]

    def fees(self, pair):
        r = self.query_private('TradeVolume', {'pair': pair.code})['result']
        taker = r['fees']
        maker = r['fees_maker']
        # Did we get exactly one fee value back ?
        assert(len(taker) == 1 and len(maker) == 1)
        return {
            'taker': Decimal(taker.values()[0]['fee'])/100,
            'maker': Decimal(maker.values()[0]['fee'])/100
        }
