import requests, time, hmac, hashlib, money
from decimal import Decimal
from PyQt4 import Qt

name = 'CoinsBank'

from money import BTC, EUR, GBP, LTC, RUB, USD
#BTC = money.ILCCCurrency(money.BTC, 4)
#EUR = money.ILCCCurrency(money.EUR, 4)
#GBP = money.ILCCCurrency(money.GBP, 4)
#LTC = money.ILCCCurrency(money.LTC, 4)
#RUB = money.ILCCCurrency(money.RUB, 4)
#USD = money.ILCCCurrency(money.USD, 4)
currencies = [BTC, EUR, GBP, LTC, RUB, USD]

BTCEUR = money.Pair(BTC, EUR)
BTCRUB = money.Pair(BTC, RUB)
BTCUSD = money.Pair(BTC, USD)
LTCBTC = money.Pair(LTC, BTC)
LTCEUR = money.Pair(LTC, EUR)
LTCRUB = money.Pair(LTC, RUB)
LTCUSD = money.Pair(LTC, USD)
EURGBP = money.Pair(EUR, GBP)
EURUSD = money.Pair(EUR, USD)
EURRUB = money.Pair(EUR, RUB)
USDRUB = money.Pair(USD, RUB)
pairs = [
    BTCEUR, BTCRUB, BTCUSD,
    LTCBTC, LTCEUR, LTCRUB, LTCUSD,
    EURGBP, EURUSD, EURRUB,
    USDRUB
]

def ticker(pair):
    data = requests.get("https://coinsbank.com/api/public/ticker", params = {'pair': str(pair)}).json()['data']
    return {'ask': Decimal(data['sell']), 'bid': Decimal(data['buy'])}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()

    def __init__(self, pair, parent=None):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.ask = None
        self.bid = None
        self.exiting = False

    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker(self.pair)
                ask = float(data['ask'])
                if ask != self.ask:
                    self.ask = ask
                    changed = True
                bid = float(data['bid'])
                if bid != self.bid:
                    self.bid = bid
                    changed = True
            except:
                self.ask = None
                self.bid = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(5)

class Trade(object):
    def __init__(self, keyfile='bitxkey'):
        f = open(keyfile, 'r')
        self.key = f.readline().strip()
        self.secret = f.readline().strip()
        f.close()

    #def balance(self):
    #    nonce = int(time.time())
    #    params = 'key={}&method=getInfo&nonce={}'.format(self.key, nonce)
    #    sign = hmac.new(self.secret, params, hashlib.sha512).hexdigest()
    #    data = {'key': self.key, 'method': 'getInfo', 'nonce': "{}".format(nonce), 'sign': sign}
    #    print data
    #    res = requests.post('https://coinsbank.com/api', data)
    #    return res
    #    #return res['data']['funds']

    def balances(self):
        #TODO: Implement balances() when API is available
        return []
