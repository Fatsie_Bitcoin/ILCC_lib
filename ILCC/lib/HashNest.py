import requests, money, hashlib, hmac
from decimal import Decimal
from time import time

name = "HashNest"

from money import BTC, LTC
currencies = [BTC, LTC]
#TODO: DOGE
currencydict = dict([(currency.code, currency) for currency in currencies])

class Trade(object):
    def __init__(self, keyfile='hnkey'):
        f = open(keyfile, 'r')
        self.user = f.readline().strip()
        self.key = f.readline().strip()
        self.secret = f.readline().strip()
        f.close()

    def balances(self):
        nonce = int(1000*time())
        hm = hmac.new(self.secret, '{}{}{}'.format(nonce, self.user, self.key), hashlib.sha256)
        url = 'https://www.hashnest.com/api/v1/currency_accounts?access_key={}&nonce={}&signature={}'.format(
            self.key, nonce, hm.hexdigest()
        )
        balance = requests.post(url).json()
        wallets = []
        for row in balance:
            try:
                currency = currencydict[row['currency']['code'].upper()]
            except:
                # Ignore unhandled currencies
                continue

            wallets += [
                money.Wallet('available', money.Money(Decimal(row['amount']), currency)),
                money.Wallet('locked', money.Money(Decimal(row['blocked']), currency))
            ]

        return wallets

