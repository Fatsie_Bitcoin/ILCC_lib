import requests, money, hashlib, struct
from time import time
from decimal import Decimal

name = "BW"

from money import BTC, LTC, ETH
currencies = [BTC, LTC, ETH]

#TODO: Implement GHS trading as commodity

# Copied from BW.com site, with some changes.
def _fill(value, length, fillByte):
    if len(value) >= length:
        return value
    else:
        fillSize = length - len(value)
    return value + chr(fillByte) * fillSize

def _doXOr(s, value):
    slist = list(s)
    for index in xrange(len(slist)):
        slist[index] = chr(ord(slist[index]) ^ value)
    return "".join(slist) 

def _hmacSign(datas, key):
    keyb = struct.pack("%ds" % len(key), key)
    value = struct.pack("%ds" % len(datas), datas)
    k_ipad = _doXOr(keyb, 0x36)
    k_opad = _doXOr(keyb, 0x5c)
    k_ipad = _fill(k_ipad, 64, 54)
    k_opad = _fill(k_opad, 64, 92)
    m = hashlib.md5()
    m.update(k_ipad)
    m.update(value)
    dg = m.digest()

    m = hashlib.md5()
    m.update(k_opad)
    subStr = dg[0:16]
    m.update(subStr)
    dg = m.hexdigest()
    return dg

def _getSignature(userName, key, nonce, secret):
    data = userName + key + nonce
    return _hmacSign(data, secret)

class Trade(object):
    def __init__(self, keyfile='bwkey'):
        f = open(keyfile, 'r')
        self.user = f.readline().strip()
        self.key = f.readline().strip()
        self.secret = f.readline().strip()
        f.close()
        
    def request(self, method='account', currency=None):
        nonce = str(int(time()))
        data = {
            'userName': self.user, 'key': self.key, 'nonce': nonce,
            'signature': _getSignature(self.user, self.key, nonce, self.secret)
        }
        if currency is not None:
            data['coint'] = currency.code.lower()
        return requests.post("https://www.bw.com/api/"+method, data=data).json()

    def balances(self):
        """
        Gives the balances
        """
        wallets = []
        for currency in currencies:
            a = self.request(method='account', currency=currency)['datas']
            wallets += [
                money.Wallet('saving', money.Money(Decimal(a['storage']['amount']), currency)),
                money.Wallet('available', money.Money(Decimal(a['balance']), currency))
            ]
        return wallets
