import bitfinex, money, requests
from decimal import Decimal
from PyQt4 import Qt

name = 'BitFinex'

from money import BTC, ETH, LTC, USD
currencies = [BTC, ETH, LTC, USD]
currencydict = dict([(currency.code.lower(), currency) for currency in currencies])

BTCUSD = money.Pair(BTC, USD)
ETHBTC = money.Pair(ETH, BTC)
ETHUSD = money.Pair(ETH, USD)
LTCBTC = money.Pair(LTC, BTC)
LTCUSD = money.Pair(LTC, USD)

client = bitfinex.Client()

def ticker(pair):
    data = client.ticker(str(pair))
    return {'ask': Decimal(data['ask']), 'bid': Decimal(data['bid'])}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()
    
    def __init__(self, pair, parent=None):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.ask = None
        self.bid = None
        self.exiting = False

    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker(self.pair)
                ask = float(data['ask'])
                if ask != self.ask:
                    self.ask = ask
                    changed = True
                bid = float(data['bid'])
                if bid != self.bid:
                    self.bid = bid
                    changed = True
            except:
                self.ask = None
                self.bid = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(5)

class Trade(bitfinex.TradeClient):
    def __init__(self, keyfile='bitfinexkey'):
        f = open(keyfile, 'r')
        key = f.readline().strip()
        secret = f.readline().strip()
        f.close()
        bitfinex.TradeClient.__init__(self, key, secret)

    def balances(self):
        bs = bitfinex.TradeClient.balances(self)
        wallets = []
        for b in bs:
            currency = currencydict[b['currency']]
            name = b['type']
            total = Decimal(b['amount'])
            avail = Decimal(b['available'])
            locked = total - avail
            wallets.append(money.Wallet(
                "{}:available".format(name),
                money.Money(avail, currency)
            ))
            wallets.append(money.Wallet(
                "{}:locked".format(name),
                money.Money(locked, currency)
            ))
        return wallets

    def fees(self, pair):
        # pair unused
        payload = {
            "request": "/v1/account_infos",
            "nonce": self._nonce
        }

        signed_payload = self._sign_payload(payload)
        r = requests.post(self.URL + "/account_infos", headers=signed_payload, verify=True)
        fees = r.json(parse_float=Decimal)[0]
        return {'maker': Decimal(fees['maker_fees'])/100, 'taker': Decimal(fees['taker_fees'])/100}
