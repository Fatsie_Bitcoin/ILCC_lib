import btceapi, money
from decimal import Decimal
from PyQt4 import Qt

name = 'BTC-e'

from money import BTC, EUR, LTC, NMC, RUB, USD
currencies = [BTC, EUR, LTC, NMC, RUB, USD]
#TODO: ppc, nvc

BTCEUR = money.Pair(BTC, EUR, 'btc_eur')
BTCRUB = money.Pair(BTC, RUB, 'btc_rub')
BTCUSD = money.Pair(BTC, USD, 'btc_usd')
EURRUB = money.Pair(EUR, RUB, 'eur_rub')
EURUSD = money.Pair(EUR, USD, 'eur_usd')
LTCBTC = money.Pair(LTC, BTC, 'ltc_btc')
LTCEUR = money.Pair(LTC, EUR, 'ltc_eur')
LTCRUB = money.Pair(LTC, RUB, 'ltc_rub')
LTCUSD = money.Pair(LTC, USD, 'ltc_usd')
NMCUSD = money.Pair(NMC, USD, 'nmc_usd')
NMCBTC = money.Pair(NMC, BTC, 'nmc_btc')
USDRUB = money.Pair(USD, RUB, 'usd_rub')
pairs = [
    BTCEUR, BTCRUB, BTCUSD,
    EURRUB, EURUSD,
    LTCBTC, LTCEUR, LTCRUB, LTCUSD,
    NMCUSD, NMCBTC,
    USDRUB
]
#TODO: ppc_usd, nvc_usd, nvc_btc, ppc_btc
pairdict = dict([(pair.code, pair) for pair in pairs])

def ticker(pair):
    data = btceapi.getTicker(str(pair))
    return {'ask': Decimal(data.buy), 'bid': Decimal(data.sell)}

class Thread(Qt.QThread):
    updated = Qt.pyqtSignal()

    def __init__(self, pair, parent=None):
        Qt.QThread.__init__(self, parent)
        self.pair = pair
        self.ask = None
        self.bid = None
        self.exiting = False

    def run(self):
        while not self.exiting:
            changed = False
            try:
                data = ticker(self.pair)
                ask = float(data['ask'])
                if ask != self.ask:
                    self.ask = ask
                    changed = True
                bid = float(data['bid'])
                if bid != self.bid:
                    self.bid = bid
                    changed = True
            except:
                self.ask = None
                self.bid = None
                changed = True
            if changed:
                self.updated.emit()
            self.sleep(5)


class Order(money.Order):
    def __init__(self, order):
        self.txid = order.order_id
        pair = pairdict[order.pair]
        left = money.Money(order.amount, pair.left)
        price = money.Money(order.rate, pair.right)
        right = money.Money((left.amount*price.amount).quantize(left.amount), pair.right)
        if order.type == 'sell':
            left = -left
        else:
            right = -right
        money.Order.__init__(self, pair, left, price, right)


class Trade(btceapi.TradeAPI):
    def __init__(self, keyfile='btcekey'):
        handler = btceapi.KeyHandler(keyfile)
        key = handler.getKeys()[0]
        btceapi.TradeAPI.__init__(self, key, handler)

    def balances(self):
        info = self.getInfo()
        return [
            money.Wallet('Total', money.Money(info.balance_btc, BTC)),
            money.Wallet('Total', money.Money(info.balance_eur, EUR)),
            money.Wallet('Total', money.Money(info.balance_ltc, LTC)),
            money.Wallet('Total', money.Money(info.balance_nmc, NMC)),
            money.Wallet('Total', money.Money(info.balance_rur, RUB)),
            money.Wallet('Total', money.Money(info.balance_usd, USD))
        ]
        # TODO: ppc, nvc

    def openorders(self):
        orders = self.activeOrders()
        return [Order(order) for order in orders]

    def fees(self, pair):
        fee = btceapi.getTradeFee(pair.code)/100
        return {'taker': fee, 'maker': fee}
