"""
The ILCC (I Love CryptoCurrencies) money classes.
These classes are used by the service provider modules that provide an interface
to online wallet, exchange, mining and other services.
"""

from moneyed import *
from decimal import ROUND_HALF_UP

# Add cryptocurrencies
BTC = add_currency('BTC', 'Nil', 'Bitcoin', [])
DASH = add_currency('DASH', 'Nil', 'Dash', [])
ETH = add_currency('ETH', 'Nil', 'Ethereum', [])
LTC = add_currency('LTC', 'Nil', 'Litecoin', [])
NMC = add_currency('NMC', 'Nil', 'Namecoin', [])
NXT = add_currency('NXT', 'Nil', 'NXT', [])

#TODO: Should we make distinction between commodity and currency ?
#TODO: each of the Cloud Mining may have different types of GHS mining. How to handle ?
#BGHS = add_currency('BGHS', 'Nil', 'Bitcoin GH/s hashing', [])

class Pair(object):
    """
    A pair of currencies for trading.
    """

    def __init__(self, left, right, code=None):
        """
        left, right: Left and right currencies from the pair.
        sep: separator inserted between the two currency strings for string representation
             of the pair. This allows to let the string representation reflect the 
             service provider's convention
        code: The whole string code for the pair. This overrules sep parameter.

        The digits for the currency in a pair can differ from the service provider default
        number of digits.
        """
        assert(
            isinstance(left, Currency)
            and isinstance(right, Currency)
        )
        self.left = left
        self.right = right
        self.code = left.code+right.code if code is None else code

    def __eq__(self, other):
        """
        A pair is considered equal if both currencies are the same; digit accuracy is ignored
        """
        return (self.left == other.left) and (self.right == self.right)

    def __str__(self):
        return self.code

    def __repr__(self):
        return "{}: {}".format(self.__class__.__name__, self.__str__())


class Wallet(object):
    """
    A class to represent a Wallet
    A wallet has a name and amount of money of class Money.
    """
    def __init__(self, name, balance):
        assert(isinstance(balance, Money))
        self.name = name
        self.balance = balance

    def __str__(self):
        return "{}: {}{}".format(self.name, self.balance.amount, self.balance.currency.code)

    def __repr__(self):
        return "<Wallet>{}".format(self.__str__())


# TODO: Support more than one fee currency ?
class Order(object):
    """
    An order for a pair. Can be in a state of executed or not executed
    """

    def __init__(self, pair, left, price, right, fee=None, executiontime=None):
        """
        We let the computation of size, output and fee to the user to account for rounding
        of exchange.
        For accounting, left and right are to be added to the balance as is, so they have to
        have opposite sign.
        Fee has to be of type Money and can be in other currencies that the one from the
        pair. The fee is not included left or right amounts. To compute balances it has to
        handled extra to left and right; e.g. after accounting left and right also fee has to be accounted for.
        """
        assert(
            isinstance(pair, Pair)
            and isinstance(left, Money) and left.currency == pair.left
            and isinstance(price, Money) and price.currency == pair.right
            and isinstance(right, Money) and right.currency == pair.right
            and ((fee is None) or isinstance(fee, Money))
        )
        self.pair = pair
        self.left = left
        self.price = price
        self.right = right
        self.fee = fee
        self.executiontime = executiontime

    def __str__(self):
        # TODO Properly handle unicode currency symbols
        return "{} {}{} @ {}{}".format(
            "Buy" if self.left.amount > 0 else "Sell",
            abs(self.left.amount), self.left.currency,
            self.price.amount, self.price.currency
        )

    def __repr__(self):
        return "{}: {}".format(self.__class__.__name__, self.__str__())
