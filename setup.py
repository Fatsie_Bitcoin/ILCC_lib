#!/usr/bin/env python2.7
from setuptools import setup, find_packages

setup(
    name = "ILCC_lib",
    version = "0.1-dev",
    packages = ["ILCC", "ILCC.lib"]
)
